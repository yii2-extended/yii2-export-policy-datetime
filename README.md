# yii2-extended/yii2-export-policy-datetime
An implementation of the yii2 export policy interface that creates files for
each models and one of their date field values.

![coverage](https://gitlab.com/yii2-extended/yii2-export-policy-datetime/badges/master/pipeline.svg?style=flat-square) 
![build status](https://gitlab.com/yii2-extended/yii2-export-policy-datetime/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar yii2-extended/yii2-export-policy-datetime ^8`


## Basic Usage

This library may be used the following way :

```php

use Yii2Extended\Yii2Export;

$policy = new Yii2ExportPolicyMonth($folderPath, Record::find(), 'date_field');

foreach($policy as $activeQuery)
{
	/* @var $activeQuery \yii\db\ActiveQuery[Record] */
	file_put_contents($policy->getCurrentFilePath().'.txt', var_export($activeQuery->all(), true));
}

```


## License

MIT (See [license file](LICENSE)).
