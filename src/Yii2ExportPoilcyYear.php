<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-export-policy-datetime library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Yii2Export;

use DateInterval;
use DateMalformedStringException;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use InvalidArgumentException;
use RuntimeException;
use yii\db\ActiveQueryInterface;
use yii\db\Connection;

/**
 * Yii2ExportPolicyYear class file.
 * 
 * This policy defines the files to put the records in with one file per year.
 * 
 * @author Anastaszor
 * @psalm-suppress PropertyNotSetInConstructor
 */
class Yii2ExportPoilcyYear implements Yii2ExportPolicyInterface
{
	
	/**
	 * The path of the folder in which are stored the files.
	 * 
	 * @var string
	 */
	protected string $_folderPath;
	
	/**
	 * The query that is the base to get records.
	 * 
	 * @var ActiveQueryInterface
	 */
	protected ActiveQueryInterface $_baseQuery;
	
	/**
	 * The name of the field in which the date value is present.
	 * 
	 * @var string
	 */
	protected string $_dateField;
	
	/**
	 * The format in which the date field is formatted. This format is the
	 * php `date()` format.
	 * 
	 * @var string
	 */
	protected string $_dateFormat;
	
	/**
	 * The minimum date from which to put records in.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_minDate;
	
	/**
	 * The maximum date from which to put records in.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_maxDate;
	
	/**
	 * The database connection, if not standard.
	 * 
	 * @var ?Connection
	 */
	protected ?Connection $_db = null;
	
	/**
	 * The current key of the iterator.
	 * 
	 * @var integer
	 */
	protected int $_currentKey = 0;
	
	/**
	 * The current date of the iterator.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_currentDate = null;
	
	/**
	 * The current query.
	 * 
	 * @var ?ActiveQueryInterface
	 */
	protected ?ActiveQueryInterface $_currentQuery = null;
	
	/**
	 * Builds a new Yii2ExportPolicyYear object with the given folder path,
	 * base query and date field of the model.
	 * 
	 * @param string $folderPath
	 * @param ActiveQueryInterface $baseQuery
	 * @param string $dateField
	 * @param string $dateFormat
	 * @param ?Connection $database
	 * @throws InvalidArgumentException if the given folder path does not
	 *                                  exists, or is not a directory, or is not writable
	 */
	public function __construct(string $folderPath, ActiveQueryInterface $baseQuery, string $dateField, string $dateFormat = 'Y-m-d', ?Connection $database = null)
	{
		$realPath = \realpath($folderPath);
		if(false === $realPath)
		{
			throw new InvalidArgumentException(\strtr('The given path {path} does not points to a real directory.', ['{path}' => $folderPath]));
		}
		
		if(!\is_dir($realPath))
		{
			throw new InvalidArgumentException(\strtr('The resolved path {path} is not a directory.', ['{path}' => $realPath]));
		}
		
		if(!\is_writable($realPath))
		{
			throw new InvalidArgumentException(\strtr('The resolved path {path} is not writeable.', ['{path}' => $realPath]));
		}
		
		$this->_folderPath = $realPath;
		$this->_baseQuery = $baseQuery;
		$this->_dateField = $dateField;
		$this->_dateFormat = $dateFormat;
		$this->_db = $database;
		
		// test the date format
		$now = new DateTimeImmutable();
		$formatted = $now->format($dateFormat);
		if(empty($formatted))
		{
			throw new InvalidArgumentException(\strtr('The format {format} is not a valid php export format.', ['{format}' => $dateFormat]));
		}
		
		$date = DateTimeImmutable::createFromFormat($dateFormat, $formatted);
		if(empty($date))
		{
			throw new InvalidArgumentException(\strtr('The format {format} is not a valid php import format.', ['{format}' => $dateFormat]));
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 * @throws RuntimeException
	 */
	public function current() : ActiveQueryInterface
	{
		if(null === $this->_currentQuery)
		{
			throw new RuntimeException('No current query selected.');
		}
		
		return $this->_currentQuery;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 * @throws RuntimeException
	 */
	public function next() : void
	{
		if(null === $this->_currentDate)
		{
			throw new RuntimeException('No current date selected.');
		}
		
		$this->_currentKey++;
		/** @var DateInterval $interval */
		$interval = DateInterval::createFromDateString('+1 year');
		$this->_currentQuery = clone $this->_baseQuery;
		
		try
		{
			$this->_currentDate = (new DateTimeImmutable('@'.((string) $this->_currentDate->getTimestamp())))->add($interval);
			$this->_currentQuery->andWhere([$this->_dateField => $this->_currentDate->format($this->_dateFormat)]);
		}
		catch(Exception|DateMalformedStringException $e)
		{
			// nothing to do
			// php>8 : throws Exception
			// php>8.3 : throws DateMalformedStringException
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return $this->_currentKey;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return null !== $this->_currentDate
			&& 0 >= $this->_currentDate->getTimestamp() - $this->_maxDate->getTimestamp();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 * @throws RuntimeException
	 */
	public function rewind() : void
	{
		$minRecord = (clone $this->_baseQuery)->addOrderBy([$this->_dateField => 'ASC'])->one($this->_db);
		if(null === $minRecord || \is_array($minRecord))
		{
			throw new RuntimeException('The as array attribute must be unset when the active query is provided.');
		}
		
		$minDateValue = (string) $minRecord->getAttribute($this->_dateField);
		$minDate = DateTimeImmutable::createFromFormat($this->_dateFormat, $minDateValue);
		if(empty($minDate))
		{
			throw new RuntimeException(\strtr('Failed to parse minimum date value {value} with format {format}', ['{value}' => $minDateValue, '{format}' => $this->_dateFormat]));
		}
		
		$this->_minDate = $minDate;
		$maxRecord = (clone $this->_baseQuery)->addOrderBy([$this->_dateField => 'DESC'])->one($this->_db);
		if(null === $maxRecord || \is_array($maxRecord))
		{
			throw new RuntimeException('The as array attribute must be unset when the active query is provided.');
		}
		
		$maxDateValue = (string) $maxRecord->getAttribute($this->_dateField);
		$maxDate = DateTimeImmutable::createFromFormat($this->_dateFormat, $maxDateValue);
		if(empty($maxDate))
		{
			throw new RuntimeException(\strtr('Failed to parse maximum date value {value} with format {format}', ['{value}' => $maxDateValue, '{format}' => $this->_dateFormat]));
		}
		
		$this->_maxDate = $maxDate;
		$this->_currentKey = 0;
		$this->_currentDate = clone $this->_minDate;
		$this->_currentQuery = clone $this->_baseQuery;
		$this->_currentQuery->andWhere([$this->_dateField => $this->_currentDate->format($this->_dateFormat)]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Yii2Export\Yii2ExportPolicyInterface::getCurrentQueryId()
	 * @throws RuntimeException
	 */
	public function getCurrentQueryId() : string
	{
		if(null === $this->_currentDate)
		{
			throw new RuntimeException('No current date selected.');
		}
		/** @phpstan-ignore-next-line */ /** @psalm-suppress NoInterfaceProperties */
		return \trim((string) \preg_replace('#\\W+#', '-', (string) $this->_baseQuery->modelClass)).'_'.$this->_currentDate->format('Y');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Yii2Export\Yii2ExportPolicyInterface::getCurrentActiveQuery()
	 * @throws RuntimeException
	 */
	public function getCurrentActiveQuery() : ActiveQueryInterface
	{
		if(null === $this->_currentQuery)
		{
			throw new RuntimeException('No current query selected.');
		}
		
		return $this->_currentQuery;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Yii2Export\Yii2ExportPolicyInterface::getCurrentPath()
	 */
	public function getCurrentPath() : string
	{
		if(null === $this->_currentDate)
		{
			throw new RuntimeException('No current date selected.');
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress NoInterfaceProperties */
		$primaryModel = new $this->_baseQuery->modelClass();
		/** @phpstan-ignore-next-line */ /** @psalm-suppress MixedMethodCall */
		$tableName = (string) $primaryModel->tableName();
		$tableSlug = \trim((string) \preg_replace('#\\W+#', '_', $tableName), '_');
		$year = $this->_currentDate->format('Y');
		
		return $this->_folderPath.\DIRECTORY_SEPARATOR.$year.'_'.$tableSlug;
	}
	
}
