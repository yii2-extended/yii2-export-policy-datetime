<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-export-policy-datetime library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;
use yii\db\ActiveQueryInterface;
use Yii2Extended\Yii2Export\Yii2ExportPoilcyYear;

/**
 * Yii2ExportPoilcyYearTest test file.
 * 
 * @author Anastaszor
 * @covers \Yii2Extended\Yii2Export\Yii2ExportPoilcyYear
 *
 * @internal
 *
 * @small
 */
class Yii2ExportPoilcyYearTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Yii2ExportPoilcyYear
	 */
	protected Yii2ExportPoilcyYear $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Yii2ExportPoilcyYear(
			__DIR__,
			$this->getMockForAbstractClass(ActiveQueryInterface::class),
			'dateField',
		);
	}
	
}
